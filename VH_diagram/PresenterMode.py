from enum import Enum, auto

class PresenterMode(Enum):
    """The different view for the Presenter."""

    DIAGRAM = auto()
    REDUCED = auto()
    SIMPLIFIED = auto()
    DETAILED = auto()
