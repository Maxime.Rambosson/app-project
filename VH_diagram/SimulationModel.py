import pandas as pd
import os

class SimulationModel:
    """Model containing the dataset of simulations."""

    def __init__(self, filename, path="./"):
        self._df = None
        self.path = path
        self.filename = filename

    def load_csv(self):
        """Load dataframe as CSV with .gz compression."""

        self._df = pd.read_csv(
            os.path.join(self.path, self.filename), compression="gzip", low_memory=False
        )

    def get_by_binary_index(self, index):
        """Return a copy of dataframe's slice with binary_index == index

        Parameters
        ----------
        index : int
            Binary index wanted.

        Returns
        -------
        pandas.DataFrame
            Copy of a sub-dataframe with binary_index == index

        """
        return self._df[self._df["binary_index"] == index].copy()
