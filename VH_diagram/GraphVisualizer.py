from PyQt5.QtWidgets import (
    QWidget,
    QFrame,
    QHBoxLayout,
    QVBoxLayout,
    QLabel,
    QGridLayout,
    QSizePolicy,
)
from PyQt5.QtCore import Qt, QPoint, QSize
from PyQt5.QtGui import QPainter, QBrush, QPixmap

from .MathTextLabel import MathTextLabel

from dataclasses import dataclass
from enum import Enum, auto


class INFOSTYPE(Enum):
    """Enumeration of different type of infos."""

    CASE = auto()
    POINT = auto()
    STATE = auto()


@dataclass
class Infos:
    """Mother-class with common informations uselful for all widget."""

    collumn_id: int
    infos_type: INFOSTYPE
    connected: bool

    def __init__(self, collumn_id, infos_type, connected):
        self.collumn_id = collumn_id
        self.infos_type = infos_type
        self.connected = connected


@dataclass
class CaseInfos(Infos):
    """Informations to create a case widget."""

    centered_text: str
    bot_right_text: str
    bot_left_text: str
    top_right_text: str
    top_left_text: str

    border_width: int

    def __init__(
        self,
        collumn_id,
        *,
        centered_txt: str = "",
        bot_right_txt: str = "",
        bot_left_txt: str = "",
        top_right_txt: str = "",
        tot_left_txt: str = "",
        border_width: int = 0,
    ):
        super(CaseInfos, self).__init__(collumn_id, INFOSTYPE.CASE, True)

        self.centered_text = centered_txt
        self.bot_right_text = bot_right_txt
        self.bot_left_text = bot_left_txt
        self.top_right_text = top_right_txt
        self.top_left_text = tot_left_txt
        self.border_width = border_width


@dataclass
class PointInfos(Infos):
    """Informations to create a widget with a point drew."""

    text: str

    def __init__(self, collumn_id, text: str = ""):
        super(PointInfos, self).__init__(collumn_id, INFOSTYPE.POINT, True)

        self.text = text


class GraphVisualizerItem(QWidget):
    """Mother-class for widget in GraphVisualizer with commun atributs, and commun method

    Attributes
    ----------
    connected : bool
        Indicate if this widget need to be connected with the previous one in the same collumn.

    """

    def __init__(self):
        super(GraphVisualizerItem, self).__init__()
        self.connected = False

    def get_attach_point_top(self):
        """Get coordinate to connect this widget with the privious one.

        Returns
        -------
        int
            Coordinate to link the widget with the previous one.

        """
        return self.mapToGlobal(self.rect().center())  # Default behavior

    def get_attach_point_bot(self):
        """Get coordinate to connect this widget with the next one.

        Returns
        -------
        int
            Coordinate to link the widget with the next one.

        """
        return self.mapToGlobal(self.rect().center())  # Default behavior


class GraphVisualizerCase(GraphVisualizerItem):
    """Case widget in GraphVisualizer, with possibility to display 5 texts :
    2 on the top 2 on the bottom, and 1 in center.
    Can have a border"""

    def __init__(self):
        super(GraphVisualizerCase, self).__init__()

        self._v_layout = QVBoxLayout()
        self.setLayout(self._v_layout)

        top_label_layout = QHBoxLayout()
        self._v_layout.addLayout(top_label_layout)

        self._top_left_label = MathTextLabel()
        top_label_layout.addWidget(self._top_left_label)
        self._top_right_label = MathTextLabel()
        top_label_layout.addWidget(self._top_right_label)

        self._center_widget = QFrame()
        self._center_text_label = QLabel()
        self._center_text_label.setAlignment(Qt.AlignCenter)
        layout = QHBoxLayout()
        layout.addWidget(self._center_text_label)
        self._center_widget.setLayout(layout)

        self._v_layout.addWidget(self._center_widget)

        bot_label_layout = QHBoxLayout()
        self._v_layout.addLayout(bot_label_layout)

        self._bot_left_label = MathTextLabel()
        bot_label_layout.addWidget(self._bot_left_label)
        self._bot_right_label = MathTextLabel()
        bot_label_layout.addWidget(self._bot_right_label)

    def set_central_text(self, text):
        self._center_text_label.setText(text)

    def set_bottom_left_text(self, text):
        self._bot_left_label.setText(text)

    def set_bottom_rigth_text(self, text):
        self._bot_right_label.setText(text)

    def set_top_left_text(self, text):
        self._top_left_label.setText(text)

    def set_top_right_text(self, text):
        self._top_right_label.setText(text)

    def set_central_border(self, width):
        self._center_widget.setFrameShape(QFrame.Box)
        self._center_widget.setLineWidth(width)

    def get_attach_point_top(self):
        return self._center_widget.mapToGlobal(
            self._center_widget.rect().topLeft()
        ) + QPoint(self._center_widget.width() / 2, 0)

    def get_attach_point_bot(self):
        return self._center_widget.mapToGlobal(
            self._center_widget.rect().bottomLeft()
        ) + QPoint(self._center_widget.width() / 2, 0)


def prepare_case(infos: CaseInfos):
    """Helper to create GraphVisualizerCase from CaseInfos

    Parameters
    ----------
    infos : CaseInfos
        Infos to create the widget.

    Returns
    -------
    GraphVisualizerCase
        Created widget with given infos.

    """
    case = GraphVisualizerCase()

    case.connected = infos.connected

    case.set_central_text(infos.centered_text)

    case.set_bottom_left_text(infos.bot_left_text)
    case.set_bottom_rigth_text(infos.bot_right_text)
    case.set_top_left_text(infos.top_left_text)
    case.set_top_right_text(infos.top_right_text)
    if infos.border_width != 0:
        case.set_central_border(infos.border_width)

    return case


class GraphVisualizerPointDraw(QWidget):
    """Empty widget with a point drew"""

    def __init__(self):
        super(GraphVisualizerPointDraw, self).__init__()

        self.setMinimumSize(QSize(13, 13))
        self.setMaximumSize(QSize(13, 13))

    def paintEvent(self, event):  # override paintEvent of QWidget
        painter = QPainter(self)

        painter.drawEllipse(self.rect().center(), 6, 6)

        painter.setBrush(Qt.black)

        painter.drawEllipse(self.rect().center(), 2, 2)


class GraphVisualizerPoint(GraphVisualizerItem):
    """Widget containing GraphVisualizerPointDraw,
    with possibility to display 2 texts : one at each side."""

    def __init__(self):
        super(GraphVisualizerPoint, self).__init__()
        self._layout = QGridLayout()
        self.setLayout(self._layout)

        self._point_draw = GraphVisualizerPointDraw()
        self._layout.addWidget(self._point_draw, 0, 0)

        self._label = MathTextLabel()
        self._layout.addWidget(self._label, 0, 1)

    def set_text(self, text):
        self._label.setText(text)

    def get_attach_point_top(self):
        return self._point_draw.mapToGlobal(self._point_draw.rect().center())

    def get_attach_point_bot(self):
        return self._point_draw.mapToGlobal(self._point_draw.rect().center())


def prepare_point(infos: PointInfos):
    """Helper to create GraphVisualizerPoint from PointInfos

    Parameters
    ----------
    infos : PointInfos
        Infos to create the widget.

    Returns
    -------
    GraphVisualizerPoint
        Created widget with given infos.

    """

    point = GraphVisualizerPoint()

    point.connected = infos.connected

    point.set_text(infos.text)

    return point


@dataclass
class StateInfos(Infos):
    """Informations to create a widget with a line of diagram inside."""

    S1_filename: str
    S2_filename: str
    distance: int
    top_texts: list
    bot_texts: list

    def __init__(
        self,
        collumn_id,
        *,
        S1_filename=None,
        S2_filename=None,
        event_filename=None,
        distance=1,
    ):
        super(StateInfos, self).__init__(collumn_id, INFOSTYPE.STATE, False)

        self.distance = distance
        self.S1_filename = S1_filename
        self.S2_filename = S2_filename
        self.event_filename = event_filename
        self.top_texts = []
        self.bot_texts = []


class GraphVisualizerState(GraphVisualizerItem):
    """Widget containing drawings,
    with possibility to display 4 texts on top & bot."""

    _max_height = 128
    _offset = 10

    def __init__(self):
        super(GraphVisualizerState, self).__init__()

        self.done = False

        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        self._bg_container = QLabel()
        self._bg_container.resize(self._max_height, self._max_height)
        layout.addWidget(self._bg_container, 1, 0, 1, 4)

        self._bg = QPixmap(self._max_height, self._max_height)
        self._bg_container.setPixmap(self._bg)

        self._S1_pixmap = QPixmap()
        self._S2_pixmap = QPixmap()
        self._event_pixmap = QPixmap()

        self._distance = 1

        self._top_labels = []
        self._bot_labels = []
        for i in range(4):
            self._top_labels.append(MathTextLabel(self))
            layout.addWidget(self._top_labels[i], 0, i, alignment=Qt.AlignCenter)

            self._bot_labels.append(MathTextLabel(self))
            layout.addWidget(self._bot_labels[i], 2, i, alignment=Qt.AlignCenter)

    def set_first_star(self, filename):
        """Load the picture 'filename' and resize it if needed

        Parameters
        ----------
        filename : str
            Name (+ path) to the picture.

        Returns
        -------
        bool
            Indicate if loading & resize success.

        """
        if not self._S1_pixmap.load(filename):
            return False

        if self._S1_pixmap.height() > self._max_height:
            self._S1_pixmap = self._S1_pixmap.scaledToHeight(self._max_height)

        return True

    def set_second_star(self, filename):
        """Load the picture 'filename' and resize it if needed

        Parameters
        ----------
        filename : str
            Name (+ path) to the picture.

        Returns
        -------
        bool
            Indicate if loading & resize success.

        """
        if not self._S2_pixmap.load(filename):
            return False

        if self._S2_pixmap.height() > self._max_height:
            self._S2_pixmap = self._S2_pixmap.scaledToHeight(self._max_height)

        return True

    def set_event(self, filename):
        """Load the picture 'filename' and resize it if needed

        Parameters
        ----------
        filename : str
            Name (+ path) to the picture.

        Returns
        -------
        bool
            Indicate if loading & resize success.

        """
        if not self._event_pixmap.load(filename):
            return False

        if self._event_pixmap.height() > self._max_height:
            self._event_pixmap = self._event_pixmap.scaledToHeight(self._max_height)

        return True

    def set_distance(self, dist):
        if dist > 1 or dist < 0:
            print("Dist need to be normalised")

        self._distance = dist

    def set_top_text(self, text, index):
        if index >= len(self._top_labels):
            print(f"Can't set text to top label {index}")
            return

        self._top_labels[index].setText(text)

    def set_bot_text(self, text, index):
        if index >= len(self._bot_labels):
            print(f"Can't set text to bot label {index}")
            return

        self._bot_labels[index].setText(text)

    def get_attach_point_top(self):
        return self._bg_container.mapToGlobal(
            self._bg_container.rect().topLeft()
        ) + QPoint(self._bg_container.width() / 2, 0)

    def get_attach_point_bot(self):
        return self._bg_container.mapToGlobal(
            self._bg_container.rect().bottomLeft()
        ) + QPoint(self._bg_container.width() / 2, 0)

    def resizeEvent(self, event):
        self._bg_container.resize(event.size().width(), self._bg_container.height())
        # Offset is used here to keep the possibility to scale down
        self._bg = QPixmap(self._bg_container.width() - self._offset, self._max_height)

        self._bg_container.setPixmap(self._bg)

    def paintEvent(self, event):  # override paintEvent of QWidget
        self._bg.fill()
        painter = QPainter(self._bg)

        x_S1 = (self._bg.width() / 2 - self._S1_pixmap.width()) * (1 - self._distance)
        y_S1 = self._bg.height() / 2 - self._S1_pixmap.height() / 2

        painter.drawPixmap(x_S1, y_S1, self._S1_pixmap)

        x_S2 = (
            self._bg.width() / 2
            + (self._bg.width() / 2 - self._S2_pixmap.width()) * self._distance
        )
        y_S2 = self._bg.height() / 2 - self._S2_pixmap.height() / 2

        painter.drawPixmap(x_S2, y_S2, self._S2_pixmap)

        if not self._event_pixmap.isNull():
            x_event = self._bg.width() / 2 - self._event_pixmap.width() / 2

            painter.drawPixmap(x_event, 0, self._event_pixmap)

        self._bg_container.setPixmap(self._bg)


def prepare_state(infos: StateInfos):
    """Helper to create GraphVisualizerState from StateInfos.

    Parameters
    ----------
    infos : StateInfos
        Infos to create the widget.

    Returns
    -------
    GraphVisualizerState
        Created widget with given infos.

    """

    state = GraphVisualizerState()

    state.connected = infos.connected

    if infos.S1_filename:
        if not state.set_first_star(infos.S1_filename):
            print(f"Can't load {infos.S1_filename} as S1")

    if infos.S2_filename:
        if not state.set_second_star(infos.S2_filename):
            print(f"Can't load {infos.S2_filename} as S2")

    if infos.event_filename:
        if not state.set_event(infos.event_filename):
            print(f"Can't load {infos.event_filename} as event")

    state.set_distance(infos.distance)

    for i in range(len(infos.top_texts)):
        state.set_top_text(infos.top_texts[i], i)

    for i in range(len(infos.bot_texts)):
        state.set_bot_text(infos.bot_texts[i], i)

    return state


class COLLUMNTYPE(Enum):
    """Enumeration of different collumn type."""
    TIMELINE = auto()
    CONNECTED = auto()


@dataclass
class ConnectedItem:
    """Represent a visual link between 2 widgets"""
    from_item: GraphVisualizerItem
    to_item: GraphVisualizerItem

    def __init__(self, from_item: GraphVisualizerItem, to_item: GraphVisualizerItem):
        self.from_item = from_item
        self.to_item = to_item


class GraphVisualizerCollumn:
    """Mother-class of visual collumn in GraphVisualizer
    Manage one visual collumn in the QGridLayout (can take several logical collumn)"""

    def __init__(self, grid, collumn_id, collumn_span):
        """Constructor

        Parameters
        ----------
        grid : QGridLayout
            Grid where collumn take place.
        collumn_id : int
            Unique id of this collumn.
        collumn_span : int
            Number of logical collumn used.

        """
        self._collumn_id = collumn_id
        self._row_index = 0
        self._collumn_span = collumn_span

        self._grid = grid
        self._items = []

        self._create_title_label()

        self._connected_items = []
        self._last_item = None

    def set_title(self, title):
        self._title_label.setText(title)

    def add_item(self, item):
        """Add one item in the collumn, connect it to previous if needed

        Parameters
        ----------
        item : GraphVisualizerItem
            Widget to add in collumn.

        """
        self._add_widget(item)

        self._items.append(item)

        if (
            item.connected and self._last_item != None
        ):  # It's not the first item added ::
            self._connected_items.append(ConnectedItem(self._last_item, item))

        self._last_item = item

    def clear(self):
        """Delete all widget in the collumn, keep the title"""

        self._last_item = None
        self._connected_items = []

        for item in self._items:
            item.deleteLater()

        self._items = []
        self._row_index = 1

    def reset(self):
        """Delete everythings (widget + title), reset logical collumn used"""
        self._last_item = None
        self._connected_items = []

        self._title_label.deleteLater()

        for item in self._items:
            item.deleteLater()

        for i in range(self._collumn_span):
            self._grid.setColumnStretch(self._collumn_id + i, 0)

        self._items = []
        self._row_index = 0

    def skip(self):
        """Skip one row"""
        self._row_index += 1

    def get_id(self):
        return self._collumn_id

    def _create_title_label(self):
        self._title_label = QLabel()
        self._title_label.setAlignment(Qt.AlignCenter)
        self._add_widget(self._title_label)

    def _add_widget(self, widget):
        """Add widget to the logical collumn used

        Parameters
        ----------
        widget : QWidget
            Widget to add.

        """
        self._grid.addWidget(
            widget, self._row_index, self._collumn_id, 1, self._collumn_span
        )
        self._row_index += 1


class GraphVisualizerConnectedCollumn(GraphVisualizerCollumn):
    """Simple visual collumn with arrow between connected widget"""

    def __init__(self, grid, collumn_id, collumn_span=1):
        super(GraphVisualizerConnectedCollumn, self).__init__(
            grid, collumn_id, collumn_span
        )

        for i in range(collumn_span):
            self._grid.setColumnStretch(self._collumn_id + i, 1)

    def draw(self, surface):
        painter = QPainter(surface)

        for connection in self._connected_items:
            start = surface.mapFromGlobal(connection.from_item.get_attach_point_bot())
            end = surface.mapFromGlobal(connection.to_item.get_attach_point_top())

            painter.drawLine(start, end)

            left = end + QPoint(-4, -8)
            rigth = end + QPoint(4, -8)

            painter.setBrush(Qt.black)
            painter.drawPolygon(end, left, rigth)


class GraphVisualizerTimeline(GraphVisualizerCollumn):
    """Visual collumn with line between connected widget, is compressed by other collumn"""
    def __init__(self, grid, collumn_id, collumn_span=1):
        super(GraphVisualizerTimeline, self).__init__(grid, collumn_id, collumn_span)
        for i in range(collumn_span):
            self._grid.setColumnStretch(self._collumn_id + i, 0)

    def draw(self, surface):
        painter = QPainter(surface)

        for connection in self._connected_items:
            start = surface.mapFromGlobal(connection.from_item.get_attach_point_bot())
            end = surface.mapFromGlobal(connection.to_item.get_attach_point_top())

            painter.drawLine(start, end)


class GraphVisualizer(QWidget):
    """Widget used to display the different collumns and add widget in them"""
    def __init__(self):
        super(GraphVisualizer, self).__init__()

        self._layout = QGridLayout()
        self.setLayout(self._layout)

        self._next_collumn = 0

        self._collumns = []

    def add_collumn(self, collumn_type, collumn_span=1):
        """Create a collumn according collumn_type, taking collumn_span logical collumn.

        Parameters
        ----------
        collumn_type : COLLUMNTYPE
            Type of the collumn to add.
        collumn_span : int
            Nb of logical collumn used.

        Returns
        -------
        int
            ID of created collumn.

        """
        if collumn_type == COLLUMNTYPE.TIMELINE:
            self._collumns.append(
                GraphVisualizerTimeline(self._layout, self._next_collumn, collumn_span)
            )
        elif collumn_type == COLLUMNTYPE.CONNECTED:
            self._collumns.append(
                GraphVisualizerConnectedCollumn(
                    self._layout, self._next_collumn, collumn_span
                )
            )

        self._next_collumn += collumn_span

        return len(self._collumns) - 1

    def get_collumn(self, collumn_id):
        """Return visual collumn with the given id

        Parameters
        ----------
        collumn_id : int
            ID of searched collumn.

        Returns
        -------
        GraphVisualizerCollumn
            Collumn with the given ID.

        """

        if collumn_id < len(self._collumns):
            return self._collumns[collumn_id]

        return None

    def add_line(self, infos):
        """For each info in infos, create the corresponding widget and add it to the collumn with corresponding id
        (given in each info), only 1 widget per collumn by call, if one collumn haven't any widget associated, collumn skip this row

        Parameters
        ----------
        infos : Array of Infos
            Array with derivated struct of Infos to create the different widget for this line

        """

        for collumn in self._collumns:
            info = next((x for x in infos if (lambda info : info.collumn_id == collumn.get_id())(x)), None)

            if info != None:
                item = None
                if info.infos_type == INFOSTYPE.CASE:
                    item = prepare_case(info)
                elif info.infos_type == INFOSTYPE.POINT:
                    item = prepare_point(info)
                elif info.infos_type == INFOSTYPE.STATE:
                    item = prepare_state(info)

                if item != None:
                    collumn.add_item(item)
            else:
                collumn.skip()

    def clear(self):
        """Clear each collumn"""
        for col in self._collumns:
            col.clear()

    def reset(self):
        """Clear all delete each collumn"""
        for col in self._collumns:
            col.reset()

        self._next_collumn = 0
        self._collumns = []

    def paintEvent(self, event):  # override paintEvent of QWidget
        for col in self._collumns:
            col.draw(self)

    def saveAsPicture(self, filename):
        """Render this widget in a picture and save it a filename

        Parameters
        ----------
        filename : str
            Destination file of the picture.
        """

        pixmap = QPixmap(self.size())
        self.render(pixmap)

        if not pixmap.save(filename):
            print("Can't save as " + filename)
