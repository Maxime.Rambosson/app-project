# Dynamic graph representation of stellar binary system evolution

This project don't work as a stand alone and take place inside a bigger project : POSYDON.

The basic usage is : 
```py
    from posydon.visualization.VHdiagram import VHdiagram

    VHdiagram('20000_binaries.csv.gz', path='./dataset/', index=18976)
```

We can specify specials options :
```py
    VHdiagram(
        "20000_binaries.csv.gz",
        index=19628,
        presentMode=PresenterMode.DIAGRAM,
        displayMode=DisplayMode.INLINE_B,
    )
```