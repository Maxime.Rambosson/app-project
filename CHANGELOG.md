# CHANGE LOG

- S1 :  
Librairy choosen : PyQt5  
Implementing MPV pattern  
Graph visualizer as custom widget  
Graph implemented as independants collumns layout (hard coded)  

- S2 :  
Implementing custom Widget for cases  
Add Infos structure for each type of case, indirection between Graphic Librairy and Presenter  
Add classes for Collumn and derivate  
Add conversion for presented data  
Add vertical arrow for cases (conversion of local coords to global)  
Dynamic windows size scaled to the screen  
Add Timeline with custom draw (specialized Collumn)  
Graph as independants collumns layouts with different widget size lead to difficulties to keep clean lines : Swap to grid layout, Collumn manage part of the grid  

- S3 :  
Add Option Window  
Need to expose callback to Presenter : Implement VisualizerInterface, expose Visualiser widget and callback options with indirection  
Add reduce/simplified Present Mode (compress some data)  
Add MathTextLabel to display LaTeX formul  
Add LaTeX formating  

- S4 :  
Add option to save the visualisation as png  
Add/Delete collumn is hard coded in Visualizer widget :  
Refactor to manage collumn by Presenter, each collumn have a ID wich is attibued to case info to know where add case widget  
Start implementing State widget : display drawing of the state of binary star  
Need to keep centered all drawing by stars (all drawing for S1 need to be in veritcal line) : need to manage in function of draw's size  

- S5/S6 :  
Add possibility to disconnect some widget (no arrow, used for diagram widget)
Change management of data for special view (before : data formated when iterate, loose part of information, now : data saved in dictionary with custom class, allow to find max mass or eccentricity)
Add distance visualisation : stars are separted by a ratio of the real separation

- S7 :
Swap distance visualisation from period_orbite to separation data
Use log(separation)
Use new draw lib : need to scale down drawing to fit in line
Add event drawings :
Event drawing are all in one (stars + event representation), need to stretch it for distance representation
Add option to scale or not

- S8 :
Add information on top of diagram : change layout of State Widget to Grid, choose a way for many labels on 1 widget

- S9 :
Starting moving file, adapting path
Add main class for usage
Fix MathTextLabel for empty text
Fix NaN and None value on some collumns
Add possibility to compute 'separation' with other data
Add some data as conditional infos to display

- S10 :
Adding documentation in each file
Refactor reduced presentation to use the SimplifiedInfo struct and work like Diagram & Simplified view
Update screen & draw path for Linux/Mac OS
Ignore 'redirect' event
Add 'merged' & 'disrupted' indication for Diagram view

- S11 :
Update merge logic of reduce/simplify methode to keep no-detached event even if the previous & current time are the same
Update add_line() method to add only 1 widget per collumn per call, and skip this line if no widget added to this collumn
Add method to update Option showed to match Presenter state
Add different VHDiagram display mode : inside a window, or print a screen of this window inside the notebook
	Problem in inline mode : don't let the event loop of Qt run (to avoid the user to quit manually the window) don't let the draw & resize event run
	Adding default size and open window without let event loop run allow to have a good screen anyway
	(Default detected on MacOS)

- S12 :
Starting documention
Starting unit-test
Find a way to let run the event loop to render everything well even in Inline Mode : prepare an callback called 0 seconde after the start of event loop, closing the window, and taking the screen. (The bug detected on MacOS still happen, LaTeX formulla still no rendering)

- S13 :
Fix the bug detected of MacOS : Changed the method to render LaTeX formulla, direct dump the Matplotlib buffer to QPixmap
Fix a problem with doc generation : explicit type of class's member ` [str] ` canno't be represented by Sphinx, changed by ` list `
Can't create QApplication inside the current Github Action used for unit-testing 
