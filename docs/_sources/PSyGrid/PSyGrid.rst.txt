.. _PSyGrid:

###############################
The PSyGrid Object
###############################

The PSyGrid class is the "bridge" connecting MESA and POSYDON. It:

- encapsulates the data from a MESA grid in a compact form
- saves the data in an HDF5 file
- allows the user to access the data in a memory-efficient way

To use PSyGrid import it using:

.. code-block:: python

  from posydon.grids.psygrid import PSyGrid


Creating a PSyGrid object
=========================

A PSyGrid HDF5 file can be created by using the ``create()`` method on a newly
constructed PSyGrid object.

Basic example
-------------
The simplest method is to provide the paths of the folder of the MESA runs
output, and the HDF5 file to be created.

.. code-block:: python

  grid = PSyGrid()
  grid.create(MESA_runs_directory, psygrid_hdf5_path)

Now, the PSyGrid object is ready to be used for accessing and plotting data.

Output options
--------------
Use the option ``verbose`` to see more details while the PSyGrid objects is
being built.

Set ``overwrite`` to ``True`` if the HDF5 file already exists.

By default, any warnings produced are collected and shown at the end
(``warn="end"``) of the process. Use ``warn="normal"`` to see the warning
when they occur, or ``warn="suppress"`` to hide them completely.

.. code-block:: python

  grid = PSyGrid()
  grid.create(MESA_runs_directory, psygrid_hdf5_path,
              verbose=True, overwrite=True, warn="suppress")


Setting the properties of the grid
----------------------------------
PSyGrid provides a compact form of the initial MESA grid. To define how many runs should be included (e.g. for testing purposes),
and which data columns are needed, use the following code block as a guide.

.. code-block:: python

  DEFAULT_BINARY_HISTORY_COLS = ['star_1_mass', 'star_2_mass', 'period_days', 'binary_separation', 'age', 'rl_relative_overflow_1', 'rl_relative_overflow_2', 'lg_mtransfer_rate']
  DEFAULT_STAR_HISTORY_COLS = ['star_age', 'star_mass', 'he_core_mass', 'c_core_mass', 'center_h1', 'center_he4', 'c12_c12', 'center_c12', 'log_LH', 'log_LHe', 'log_LZ', 'log_Lnuc', 'surface_h1', 'log_Teff', 'log_L']
  DEFAULT_PROFILE_COLS = ['radius', 'mass', 'logRho', 'omega']

  GRIDPROPERTIES = {
      # file loading parameters
      "description": "",                      # description text
      "max_number_of_runs": None,
      "format": "hdf5",
      # resampling parameters
      "n_downsample_history": None,           # False/None = no hist. resampling
      "n_downsample_profile": None,           # False/None = no prof. resampling
      "history_resample_columns": DEFAULT_HISTORY_DOWNSAMPLE_COLS,
      "profile_resample_columns": DEFAULT_PROFILE_DOWNSAMPLE_COLS,
      # columns to pass to the grid
      "star1_history_saved_columns": DEFAULT_STAR_HISTORY_COLS,
      "star2_history_saved_columns": DEFAULT_STAR_HISTORY_COLS,
      "binary_history_saved_columns": DEFAULT_BINARY_HISTORY_COLS,
      "star1_profile_saved_columns": DEFAULT_PROFILE_COLS,
      "star2_profile_saved_columns": DEFAULT_PROFILE_COLS,
      # Initial/Final value arrays
      "initial_value_columns": None,
      "final_value_columns": None,
  }

  grid = PSyGrid()
  grid.create(MESA_runs_directory, psygrid_path, **GRIDPROPERTIES)


Loading an existing PSyGrid object
==================================

To load a PSyGrid object from an HDF5 file you only need its path:

.. code-block:: python

  grid = PSyGrid('/home/mydata/my_MESA_grid')

Closing/deleting a PSyGrid object
==================================
As with files in Python, it is preferable to "close" PSyGrid objects after
handling them.

The ``.close()`` method closes the HDF5 file associated to the PSyGrid object.
However, the object continues to exist and hold all the metadata.

.. code-block:: python

  grid.close()

In the case that a PSyGrid object is not needed anymore,
use the ``del`` keyword. Note that this also closes the HDF5 file.

.. code-block:: python

  del grid
